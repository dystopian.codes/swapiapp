// src/App.test.tsx
import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';

describe('App Component', () => {
  test('renders app component', () => {
    render(<App />);
    expect(screen.getByText('Home')).toBeInTheDocument();
  });
});




