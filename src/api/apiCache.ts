// src/api/apiCache.ts
interface Cache<T> {
    [key: string]: T;
  }
  
export const cache: Cache<any> = {};
  