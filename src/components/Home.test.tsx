// Home.test.tsx
import { render, screen } from '@testing-library/react';
import Home from './Home';

describe('Home Component', () => {
  test('renders home content', () => {
    render(<Home />);
    expect(screen.getByText('Episode X: The Challenge Awaits')).toBeInTheDocument();
  });
});
