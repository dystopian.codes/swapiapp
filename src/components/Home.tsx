import React from 'react'
import Crawl from 'react-star-wars-crawl'
import 'react-star-wars-crawl/lib/index.css'




const Home = () => {
  return (
    <Crawl
    title="Episode X: The Challenge Awaits"
    subTitle="The Quest for the FFW Squadron"
    text="Once upon a current timestamp, in a galaxy surprisingly close to home, a brave developer embarked on a grand quest to join the legendary FFW Global Forces. 
  
    With a keyboard as their sword and Google as their map, they ventured forth, facing trials of code, puzzles of logic, and the dreaded technical interview. Along the way, they encountered allies in project managers and designers, forming a fellowship united in the quest for clean code and responsive design.
    
    Will our intrepid developer conquer the challenges and secure their place among the digital knights of FFW? Only time, and perhaps a few lines of code, will tell."
  />
  

  );
};

export default Home;



