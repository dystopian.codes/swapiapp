// NavBar.test.tsx
import { render, screen } from '@testing-library/react';
import NavBar from './NavBar';
import { MemoryRouter } from 'react-router-dom';

describe('NavBar Component', () => {
  test('renders links with correct navigation paths', () => {
    render(<NavBar />, { wrapper: MemoryRouter });
    
    // Assert that the NavBar renders links with the correct href attributes
    const peopleLink = screen.getByText(/people/i).closest('a');
    expect(peopleLink).toHaveAttribute('href', '/people');
  });
});
