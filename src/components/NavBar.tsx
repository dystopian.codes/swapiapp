// src/components/NavBar.tsx
import React from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

const NavBar: React.FC = () => {
  return (
    <AppBar position="fixed">
      <Toolbar variant="dense"> 
        <Typography variant="h3" component="div" sx={{ flexGrow: 1 }}>
          <Link to="/" style={{ textDecoration: 'none', color: 'inherit' }}>Home</Link>
        </Typography>
        <Box sx={{ flexGrow: 0 }}> 
          <Typography variant="h3" component="div">
            <Link to="/people" style={{ textDecoration: 'none', color: 'inherit' }}>People</Link>
          </Typography>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
