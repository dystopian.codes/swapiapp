// NotFound.test.tsx
import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import NotFound from './NotFound';

describe('NotFound Component', () => {
  test('renders the 404 Page Not Found message', () => {
    render(<NotFound />);
    const headingElement = screen.getByRole('heading', { name: /404 Page Not Found/i });
    expect(headingElement).toBeInTheDocument();
    const paragraphElement = screen.getByText(/The page you are looking for does not exist./i);
    expect(paragraphElement).toBeInTheDocument();
  });
});
