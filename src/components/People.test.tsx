import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import People from './People';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import axios from 'axios';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

// Mock data for the people list and pagination
const mockPeopleDataPage1 = {
  data: {
    results: [
      {
        name: "Luke Skywalker",
        height: "172",
        mass: "77",
        gender: "male",
        url: "https://swapi.dev/api/people/1/"
      },
    ],
    next: "https://swapi.dev/api/people/?page=2",
    previous: null,
  },
};



describe('People Component', () => {
  beforeEach(() => {
    mockedAxios.get.mockReset();
  });

  test('renders people list and navigates to person detail', async () => {
    mockedAxios.get.mockResolvedValueOnce(mockPeopleDataPage1);
    render(
      <MemoryRouter initialEntries={['/people?page=1']}>
        <Routes>
          <Route path="/people" element={<People />} />
        </Routes>
      </MemoryRouter>
    );

    await waitFor(() => expect(screen.getByText("Luke Skywalker")).toBeInTheDocument());

    const personLink = screen.getByRole('link', { name: /Luke Skywalker/i });
    expect(personLink).toHaveAttribute('href', '/people/1');
  });

  test('paginates to the next page and finds Leia Organa', async () => {
    // Setup initial mock for page 1
    mockedAxios.get.mockResolvedValueOnce(mockPeopleDataPage1);
    
    // Render component
    render(
      <MemoryRouter initialEntries={['/people?page=1']}>
        <Routes>
          <Route path="/people" element={<People />} />
        </Routes>
      </MemoryRouter>
    );
    
    // Ensure Luke Skywalker is displayed from page 1
    await waitFor(() => expect(screen.getByText("Luke Skywalker")).toBeInTheDocument());
  

  });
  
  
});
