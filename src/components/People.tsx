import React, { useEffect, useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import queryString from 'query-string';
import { useStarWarsApi } from '../hooks/useStarWarsApi';
import { Link } from 'react-router-dom';
import { Box, Card, CardContent, Typography, Grid, Button, CardActionArea } from '@mui/material';
import HeightIcon from '@mui/icons-material/Height';
import FitnessCenterIcon from '@mui/icons-material/FitnessCenter';
import WcIcon from '@mui/icons-material/Wc';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

interface Person {
  name: string;
  height: string;
  mass: string;
  gender: string;
  url: string;
}

const People: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [currentPage, setCurrentPage] = useState(1); // Maintain currentPage as state

  const { data: { results, next, previous }, isLoading, setUrl } = useStarWarsApi<Person>(`https://swapi.dev/api/people/?page=${currentPage}`);

  useEffect(() => {
    const queryParams = queryString.parse(location.search);
    const page = parseInt(queryParams.page as string, 10) || 1;
    setCurrentPage(page); // Update currentPage state
    setUrl(`https://swapi.dev/api/people/?page=${page}`);

    if (!isLoading && results.length === 0 && page > 1) {
      navigate("/404");
    }
  }, [location.search, navigate, isLoading, results.length, setUrl]);

  const handlePagination = (newPage: number) => {
    setCurrentPage(newPage); // Update currentPage state
    navigate(`?page=${newPage}`);
  };

  if (isLoading) return <Typography>Loading...</Typography>;

  return (
    <Box sx={{ flexGrow: 1, padding: 2 }}>
      <Typography variant="h1" component="h1" gutterBottom>People</Typography>
      <Grid container spacing={2}>
        {results.map((person) => (
          <Grid item xs={12} sm={6} md={4} key={person.name}>
            <Card>
              <CardActionArea component={Link} to={`/people/${extractId(person.url)}`}>
                <CardContent>
                  <Typography variant="h2" component="h2">{person.name}</Typography>
                  <Box display="flex" alignItems="center" mt={1}>
                    <HeightIcon sx={{ mr: 1 }} />
                    <Typography variant="body1">Height: {person.height}</Typography>
                  </Box>
                  <Box display="flex" alignItems="center" mt={1}>
                    <FitnessCenterIcon sx={{ mr: 1 }} />
                    <Typography variant="body1">Mass: {person.mass}</Typography>
                  </Box>
                  <Box display="flex" alignItems="center" mt={1}>
                    <WcIcon sx={{ mr: 1 }} />
                    <Typography variant="body1">Gender: {person.gender}</Typography>
                  </Box>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
      <Box display="flex" justifyContent="center" mt={4}>
        <Button
          startIcon={<ArrowBackIosNewIcon />}
          onClick={() => previous && handlePagination(currentPage - 1)}
          disabled={!previous}
          sx={{ marginRight: 2 }}
        >
          Previous
        </Button>
        <Button
          endIcon={<ArrowForwardIosIcon />}
          onClick={() => next && handlePagination(currentPage + 1)}
          disabled={!next}
        >
          Next
        </Button>
      </Box>
    </Box>
  );
};

export default People;

function extractId(url: string): string {
  const idPattern = /\/([0-9]+)\/$/;
  const match = url.match(idPattern);
  return match ? match[1] : '0';
}
