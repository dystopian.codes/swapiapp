import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import PersonDetail from './PersonDetail';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import axios from 'axios';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({
    id: '1',
  }),
}));

describe('PersonDetail Component', () => {
  beforeEach(() => {
    mockedAxios.get.mockResolvedValue({
      data: {
        name: 'Luke Skywalker',
        height: '172',
        mass: '77',
        gender: 'male',
        birth_year: '19BBY',
        eye_color: 'blue',
        hair_color: 'blond',
        skin_color: 'fair',
      },
    });
  });

  test('fetches and displays person details correctly', async () => {
    render(
      <MemoryRouter initialEntries={['/people/1']}>
        <Routes>
          <Route path="people/:id" element={<PersonDetail />} />
        </Routes>
      </MemoryRouter>
    );

    expect(await screen.findByText('Luke Skywalker')).toBeInTheDocument();
    expect(await screen.findByText('Height: 172')).toBeInTheDocument();
    expect(await screen.findByText('Mass: 77')).toBeInTheDocument();
    expect(await screen.findByText('Gender: male')).toBeInTheDocument();
    expect(await screen.findByText('Birth Year: 19BBY')).toBeInTheDocument();
    expect(await screen.findByText('Eye Color: blue')).toBeInTheDocument();
    expect(await screen.findByText('Hair Color: blond')).toBeInTheDocument();
    expect(await screen.findByText('Skin Color: fair')).toBeInTheDocument();
  });
});
