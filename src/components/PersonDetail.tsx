import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Typography, Card, CardContent, Box } from '@mui/material';
import HeightIcon from '@mui/icons-material/Height';
import FitnessCenterIcon from '@mui/icons-material/FitnessCenter';
import WcIcon from '@mui/icons-material/Wc';
import CakeIcon from '@mui/icons-material/Cake';
import VisibilityIcon from '@mui/icons-material/Visibility';
import HairIcon from '@mui/icons-material/ContentCut';
import PaletteIcon from '@mui/icons-material/Palette'; 

// Define the shape of the person data we expect from the API
interface Person {
  name: string;
  height: string;
  mass: string;
  gender: string;
  birth_year: string;
  eye_color: string;
  hair_color: string;
  skin_color: string;
}

const PersonDetail: React.FC = () => {
  // Extract the id param from the URL
  const { id } = useParams<{ id: string }>();
  
  // State to hold the person data fetched from the API
  const [person, setPerson] = useState<Person | null>(null);

  useEffect(() => {
    // Construct the URL to fetch the person details
    const personUrl = `https://swapi.dev/api/people/${id}/`;

    // Fetch the person details from the API
    axios.get(personUrl)
      .then(response => {
        // Update the state with the fetched data
        setPerson(response.data);
      })
      .catch(error => console.error('Fetching error:', error));
  }, [id]); // Re-run the effect if the id changes

  // Show loading text while the data is being fetched
  if (!person) return <Typography variant="h5" component="h2">Loading...</Typography>;

  // Render the person details inside a card once the data is fetched
  return (
    <Card>
      <CardContent>
        <Typography variant="h1" component="h1" gutterBottom>{person.name}</Typography>
        <Box display="flex" alignItems="center">
          <HeightIcon />
          <Typography variant="body1" marginLeft={1}>Height: {person.height}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <FitnessCenterIcon />
          <Typography variant="body1" marginLeft={1}>Mass: {person.mass}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <WcIcon />
          <Typography variant="body1" marginLeft={1}>Gender: {person.gender}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <CakeIcon />
          <Typography variant="body1" marginLeft={1}>Birth Year: {person.birth_year}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <VisibilityIcon />
          <Typography variant="body1" marginLeft={1}>Eye Color: {person.eye_color}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <HairIcon />
          <Typography variant="body1" marginLeft={1}>Hair Color: {person.hair_color}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <PaletteIcon />
          <Typography variant="body1" marginLeft={1}>Skin Color: {person.skin_color}</Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

export default PersonDetail;
