import { useState, useEffect } from 'react';
import axios from 'axios';
import { cache } from '../api/apiCache'; // Import the shared cache

interface ApiResponse<T> {
  results: T[];
  next: string | null;
  previous: string | null;
}



export const useStarWarsApi = <T>(initialUrl: string) => {
  const [data, setData] = useState<ApiResponse<T>>({ results: [], next: null, previous: null });
  const [url, setUrl] = useState(initialUrl);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      if (cache[url]) {
        setData(cache[url]);
        setLoading(false);
      } else {
        try {
          const response = await axios.get<ApiResponse<T>>(url);
          cache[url] = response.data;
          setData(response.data);
        } catch (error) {
          console.error('Fetching error:', error);
        } finally {
          setLoading(false);
        }
      }
    };

    fetchData();
  }, [url]);

  return { data, isLoading, setUrl };
};
