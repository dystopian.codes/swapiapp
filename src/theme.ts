// theme.ts
import { createTheme } from '@mui/material/styles';



const theme = createTheme({
  palette: {
    mode: 'dark',
    background: {
      default: '#000', 
      paper: '#333',
    },
    text: {
      primary: '#FFEA00', 
      secondary: '#B2B2B2',
    },
    action: {
      active: '#FFEA00', 
      hover: '#B3B3B3',
    },
  },
  typography: {
    fontFamily: '"Open Sans", sans-serif',
    h1: {
      fontSize: '2.3rem',
      color: '#FFEA00',
      fontFamily: '"StarJedi", sans-serif',
      letterSpacing:1.7
    },
    h2: {
      fontSize: '1.3rem',
      color: '#FFEA00',
      fontFamily: '"StarJedi", sans-serif',
      letterSpacing:2
    },
    h3: {
      fontSize: '1rem',
      color: '#FFEA00',
      fontWeight: 700
     
    },
    body1: {
      fontSize: '1rem',
      color: '#FFEA00',
  
    },
  },
  components: {
   
    MuiCard: {
      styleOverrides: {
        root: {
          backgroundColor: '#000',
          color: '#FFEA00', 
          '&:hover': {
            backgroundColor: '#000000', 
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderColor: '#FFEA00',
          color: '#FFEA00', 
          '&:hover': {
            backgroundColor: '#424242', 
          },
        },
      },
    },
    MuiAppBar: {
      styleOverrides: {
        root: {
          backgroundColor: '#000', 
          color: '#FFEA00', 
          textTransform: 'uppercase',
          
        },
      },
    },
    
  },
});

export default theme;
